### 说明
Discuz! L1.0 目前暂停维护，后期继续维护时间另行通知，请直接下载 Discuz! X3.4 打包版使用：

https://gitee.com/3dming/DiscuzL/attach_files

### 相关网站

- Discuz! 官方站：http://www.discuz.net
- Discuz! 应用中心：http://addon.discuz.com
- Discuz! 开放平台：http://open.discuz.net
- Discuz! 粉丝网：https://www.discuzfans.net

![输入图片说明](https://images.gitee.com/uploads/images/2019/0104/142935_73eddc5c_134400.png "qqqun.png")